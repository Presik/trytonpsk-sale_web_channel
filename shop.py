# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.model import fields


class SaleShop(metaclass=PoolMeta):
    __name__ = 'sale.shop'
