
from . import rappi

from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateView, Button, StateReport,
                            StateTransition)
from trytond.report import Report
from trytond.pyson import Eval


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    is_paymented = fields.Function(fields.Boolean('Is Paymented'), 'get_is_paymented')
    products_string = fields.Char('Products String')
    # channel = fields.Many2One('sale.web_channel', 'channel')
    uploaded_invoice = fields.Boolean('uploaded Invoice', readonly=True)
    document_invoice = fields.Char('Document Invoice')
    tracking_number = fields.Char('Tracking Number')
    pack_id = fields.Char('Pack ID')

    def get_is_paymented(self, name):
        if self.residual_amount == 0:
            return True
        else:
            return False

    def get_string_lines(self, name):
        string_ = ''
        for line in self.lines:
            string_ += line.product.rec_name + ' X ' + str(line.quantity)
        return string_

    # @classmethod
    # def workflow_to_end(cls, sales):
    #     super(Sale, cls).workflow_to_end(sales)
    #     for sale in sales:
    #         if sale.channel:
    #             if sale.channel.channel_name == 'mercadolibre':
    #                 MercadoLibre = Pool().get('sale.web_channel.mercado_libre')
    #                 channel = MercadoLibre(sale.channel)
    #                 channel.upload_invoice(sale)

    @classmethod
    def create(cls, records):
        sales = super(Sale, cls).create(records)
        for sale in sales:
            string_ = ''
            for line in sale.lines:
                string_ += line.product.rec_name + ' X ' + str(line.quantity)
            cls.write([sale], {'products_string': string_})
        return sales

    @classmethod
    def quote(cls, sales):
        super(Sale, cls).quote(sales)
        for sale in sales:
            # if sale.web_shop.type == 'rappi':
            #     print('si entra aqui en quote')
            #     rappi_rec = rappi.Rappi(sale.web_shop)
            #     rappi_rec.take_order(sale.web_id, 20)
            pass

    @classmethod
    def confirm(cls, sales):
        super(Sale, cls).confirm(sales)
        for sale in sales:
            if sale.web_shop.type == 'rappi':
                print('si entra aqui en confirm')
                rappi_rec = rappi.Rappi(sale.web_shop)
                rappi_rec.ready_for_pickup(sale.web_id)
            invoice = super(Sale, cls).create_invoice(sale)
            invoice.save()

    @classmethod
    def cancel(cls, sales):
        super(Sale, cls).confirm(sales)
        for sale in sales:
            if sale.web_shop.type == 'rappi':
                print('si entra aqui en reject')
                rappi_rec = rappi.Rappi(sale.web_shop)
                rappi_rec.reject(sale.web_id)

    # def create_invoice(self):
    #     'Create and return an invoice'
    #     pool = Pool()
    #     Invoice = pool.get('account.invoice')
    #     if self.invoice_method == 'manual':
    #         return
    #
    #     invoice_lines = []
    #     sales = [self]
    #     if self.pack_id:
    #         sales = self.search([
    #             ('pack_id', '=', self.pack_id)
    #         ])
    #         self.process_pack()
    #     for sale in sales:
    #         for line in sale.lines:
    #             if self.total_amount < 0 and line.quantity > 0:
    #                 continue
    #             if self.total_amount > 0 and line.quantity < 0:
    #                 continue
    #             invoice_lines.append(line.get_invoice_line())
    #     invoice_lines = list(chain(*invoice_lines))
    #     if not invoice_lines:
    #         return
    #
    #     invoice = self._get_invoice_sale()
    #     if getattr(invoice, 'lines', None):
    #         invoice_lines = list(invoice.lines) + invoice_lines
    #     invoice.lines = invoice_lines
    #     invoice.save()
    #
    #     Invoice.update_taxes([invoice])
    #     return invoice

    @classmethod
    def delete_duplicates(cls, sale):
        cursor = Transaction().connection.cursor()
        sales_ = cls.search([('reference', '=', sale.reference)])
        if len(sales_) > 1:
            for s in sales_:
                if s.invoices or s.id == sale.id:
                    continue
                cursor.execute('DELETE FROM sale_sale where id=%s' % s.id)

    # @classmethod
    # def process(cls, sales):
    #     for sale in sales:
    #         cls.delete_duplicates(sale)
    #     super(Sale, cls).process(sales)
    #     sale = sales[0]
    #     sale.process_pack()

    def process_pack(self):
        invoice_number = self.invoice_number
        sales = []
        for inv in self.invoices:
            sales.extend(inv.sales)
        self.write(sales, {
            'state': 'processing',
            'invoice_number': invoice_number
            })


class SaleUploadInvoice(Wizard):
    'Sale Upload Invoice'
    __name__ = 'sale_web_channel.upload_invoice'
    start_state = 'upload_invoice'
    upload_invoice = StateTransition()

    def transition_upload_invoice(self):
        pool = Pool()
        Sale = pool.get('sale.sale')

        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'

        for sale in Sale.browse(ids):
            if not sale.invoices or not sale.channel:
                continue
            if sale.channel.channel_name == 'mercadolibre':
                MercadoLibre = pool.get('sale.web_channel.mercado_libre')
                channel = MercadoLibre(sale.channel)
                channel.upload_invoice(sale)
            if sale.channel.channel_name == 'shopify':
                Shopify = pool.get('sale.web_channel.shopify')
                channel = Shopify(sale.channel)
                channel.upload_invoice(sale)
        return 'end'


class SaleForChannelStart(ModelView):
    'Sale For Channel Start'
    __name__ = 'sale_web_channel.sale_for_channel.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    shop = fields.Many2One('sale.shop', 'Shop', required=True, domain=[
        ('company', '=', Eval('company'))
    ])
    channels = fields.Many2Many('sale.web_channel', None, None, 'Channel')
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    include_canceled = fields.Boolean('Include Canceled')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class SaleForChannel(Wizard):
    'Sale For Channel'
    __name__ = 'sale_web_channel.sale_for_channel'
    start = StateView('sale_web_channel.sale_for_channel.start',
                      'sale_web_channel.sale_for_channel_start_view_form', [
                        Button('Cancel', 'end', 'tryton-cancel'),
                        Button('Print', 'print_', 'tryton-ok', default=True),
                        ])
    print_ = StateReport('sale_web_channel.sale_for_channel_report')

    def do_print_(self, action):

        report_context = {
            'company': self.start.company.id,
            'company_name': self.start.company.rec_name,
            'shop': self.start.shop.id,
            'shop_name': self.start.shop.name,
            'channel': [],
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'include_canceled': self.start.include_canceled,

            }
        report_context['channels'] = [{'id': c.id, 'name': c.rec_name} for c in self.start.channels]
        return action, report_context

    def transition_print_(self):
        return 'end'


class SaleForChannelReport(Report):
    'Sale For Channel Report'
    __name__ = 'sale_web_channel.sale_for_channel_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Sale = pool.get('sale.sale')
        channel_ids = [c['id'] for c in data['channels']]
        channel_names = [c['name'] for c in data['channels']]
        shop_id = data['shop']
        shop_name = data['shop_name']
        company_name = data['company_name']

        dom_sales = [
            ('shop', '=', shop_id),
            ('sale_date', '>=', data['start_date']),
            ('sale_date', '<=', data['end_date']),

        ]
        states_sale = ['processing', 'done']
        if data['include_canceled']:
            states_sale.append('cancel')
        dom_sales.append(('state', 'in', states_sale))

        if channel_ids:
            dom_sales.append(
                ('channel', 'in', channel_ids)
            )

        sales = Sale.search(dom_sales, order=[('sale_date', 'DESC')])

        untaxed_amount_ = []
        tax_amount_ = []
        total_amount_ = []
        total_amount_ = []
        for sale in sales:
            untaxed_amount_.append(sale.untaxed_amount)
            tax_amount_.append(sale.tax_amount)
            total_amount_.append(sale.total_amount)

        report_context['records'] = sales
        report_context['total_amount'] = sum(total_amount_)
        report_context['tax_amount'] = sum(tax_amount_)
        report_context['untaxed_amount'] = sum(untaxed_amount_)
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['shop'] = shop_name
        report_context['channels'] = ', '.join(channel_names)
        report_context['company'] = company_name
        return report_context


# class Invoice(metaclass=PoolMeta):
#     __name__ = 'account.invoice'
#     salesman = fields.Many2One('company.employee', 'Salesman',
#         select=True, states={
#             'readonly': Eval('state') != 'draft',
#         })
