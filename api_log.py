# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
import simplejson as json
from trytond.pool import Pool, PoolMeta

STATES = {'readonly': True}


class ApiLog(metaclass=PoolMeta):
    "API Log"
    __name__ = "api.log"
    web_shop = fields.Many2One('web.shop', 'Web Shop', states=STATES)
    # channel = fields.Many2One('sale.web_channel', 'channel', states=STATES)
    # number = fields.Char('Number Doc', states=STATES)
    # order = fields.Char('Order', states=STATES)
    # record_date = fields.Date('Record Date', states=STATES)
    # msg_response = fields.Char('Msg Response', states=STATES)
    # request_json = fields.Text("Request Json", states=STATES)
    # headers_json = fields.Text("Header Json", states=STATES)
    # file_json = fields.Binary("File Json", states=STATES)
    # status = fields.Char('Status', states=STATES)

    @classmethod
    def __setup__(cls):
        super(ApiLog, cls).__setup__()
        cls._order.insert(0, ('record_date', 'DESC'))

    # @classmethod
    # def process_log_cron(cls):
    #     logs = cls.search([
    #         ('status', '=', 'pending')
    #     ], limit=10)
    #     for log in logs:
    #         data = log.file_json.decode("utf-8")
    #         res = {}
    #         if log.channel.channel_name == 'mercadolibre':
    #             MercadoLibre = Pool().get('sale.web_channel.mercado_libre')
    #             res = MercadoLibre.request_api(json.loads(data))
    #         cls.write([log], res)
